package me.finestdev.eCrystal;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import com.trc202.CombatTag.CombatTag;
import com.trc202.CombatTagApi.CombatTagApi;

public class Main extends JavaPlugin {
	
	static CombatTagApi combatApi;

	public void onEnable(){
		saveDefaultConfig();

		Bukkit.getPluginManager().registerEvents(new Events(this), this);
		
	}
	

	public void onDisable(){
		Events.crystals.clear();
	}
	
	public void dropCrystal(Location drop, List<ItemStack> items, Player dead){

		ItemStack crystal = new ItemStack(Material.NETHER_STAR, 1);

		ItemMeta meta = crystal.getItemMeta();
		meta.setDisplayName( getConfig().getString("Item.Name").replace("$", "�").replace("PLAYER", dead.getName()) );

		List<String> lore = new ArrayList<String>();
		lore.add( ChatColor.translateAlternateColorCodes('$', getConfig().getString("Item.Lore1")).replace("%PLAYER%", dead.getName()) );
		lore.add( ChatColor.translateAlternateColorCodes('$', getConfig().getString("Item.Lore2")).replace("%PLAYER%", dead.getName()) );
		lore.add( ChatColor.translateAlternateColorCodes('$', getConfig().getString("Item.Lore3")).replace("%PLAYER%", dead.getName()) );
		
		meta.setLore(lore);
		crystal.setItemMeta(meta);
		
		Inventory inventory = Bukkit.createInventory(dead, (54), ChatColor.translateAlternateColorCodes('$', getConfig().getString("Inventory.Title").replace("PLAYER", dead.getName())));
		
		for(ItemStack i : items){
			inventory.addItem(i);
		}
		
		
		
		drop.getWorld().dropItemNaturally(drop, crystal);
		
		Events.crystals.put(crystal.toString(), inventory);
	}
	
	public static CombatTagApi getCombaTag(){
		
		if(Bukkit.getServer().getPluginManager().getPlugin("CombatTag") != null){
			combatApi = new CombatTagApi((CombatTag) Bukkit.getServer().getPluginManager().getPlugin("CombatTag")); 
		}
		return combatApi;
		
	}
	
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
    	Player p = (Player)sender;

		if(cmd.getName().equalsIgnoreCase("eCrystals")){
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&m----------&6[&eeCrystals&6]&6&m----------"));
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Author: &cFinestDev"));
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Version: &7v&c1.0"));
			return true;
		}
    	
		return false;
    	
    }
	

}
