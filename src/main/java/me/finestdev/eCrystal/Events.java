package me.finestdev.eCrystal;

import java.util.HashMap;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.trc202.CombatTag.CombatTag;
import com.trc202.CombatTagApi.CombatTagApi;

public class Events implements Listener {

	Main pl;
	public Events(Main in){
		pl = in;
	}
	
	CombatTagApi ct = Main.getCombaTag();

	public static HashMap<String, Inventory> crystals = new HashMap<String, Inventory>();

	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		if(e.getDrops().size() == 0){ return; }
		if(e.getEntity().getKiller() instanceof Player){
			Player p = e.getEntity();

			List<ItemStack> drops = e.getDrops();

			pl.dropCrystal(p.getLocation(), drops, p);
			e.getDrops().clear();
			
			
		}
	}

	@EventHandler
	public void onOpen(PlayerInteractEvent e){
		Player p = e.getPlayer();

		ItemStack inHand = p.getItemInHand();
		
			if(inHand != null){
				if(inHand.getType() == Material.NETHER_STAR){
					if(!ct.isInCombat(p)){
					if(crystals.containsKey(inHand.toString())){
						

						p.openInventory(crystals.get(inHand.toString()));
						p.setItemInHand(null);

					}
				}else{
					p.sendMessage(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Message")));
				}
			}
		}
	}
}
